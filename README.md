Installation
------------

#### Add composer repositories
    "repositories": [
        ....
        {
            "type": "vcs",
            "url": "https://bitbucket.org/smoke686/yii2-api-logger"
        }
    ]
    
####Either run
    
    php composer.phar require --prefer-dist "jekhe/yii2-api-logger" "dev-master"

or add

    "jekhe/yii2-api-logger": "dev-master"

to the require section of your `composer.json` file.


Config
-------

### Database migrations


    yii migrate --migrationPath=@vendor/jekhe/yii2-api-logger/migrations


### Controller


```php
public function behaviors()
{
    return [
        jekhe\logger\behaviors\LoggerBehavior::class
    ];
}
```
