<?php

namespace jekhe\logger\components;


use jekhe\logger\models\ApiLog;

class Logger
{
    /**
     * @param string $controllerName
     * @param string $actionName
     * @param $result
     * @return bool
     */
    public static function log($controllerName, $actionName, $result)
    {
        $headers = \Yii::$app->request->headers;

        $params = \Yii::$app->request->getQueryParams();
        $queryParams = '';
        foreach ($params as $key => $value){
            $queryParams = (empty($queryParams) ? '' : '&'). $key."=".$value;
        }

        $requestBody = [];
        try {
            $requestBody = isset($headers['content-type']) && strpos(mb_strtolower($headers['content-type']), 'application/json') !== false ?
                json_decode(\Yii::$app->request->getRawBody(), true) : \Yii::$app->request->post();
        } catch (\Exception $exception) {
        }

        $log = new ApiLog([
            'controller' => $controllerName ?? null,
            'action' => $actionName ?? null,
            'method' => \Yii::$app->request->getMethod(),
            'statusCode' => \Yii::$app->response->statusCode ?? null,
            'queryParams' => !empty($queryParams) ? $queryParams : null,
            'requestHeaders' => !empty($headers) ? $headers : null,
            'requestBody' => !empty($requestBody) ? $requestBody : null,
            'responseBody' => is_array($result) && !empty($result) ? $result : null,
        ]);

        return $log->save();
    }
}