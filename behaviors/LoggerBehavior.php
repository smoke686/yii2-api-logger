<?php

namespace jekhe\logger\behaviors;


use yii\base\ActionEvent;
use yii\base\Behavior;
use yii\web\Controller;
use jekhe\logger\components\Logger;

/**
 * Class Logger
 * @package jekhe\logger\behaviors
 */
class LoggerBehavior extends Behavior
{
    public $loggingEvent = [];
    public $exceptEvent = [];

    /**
     * @return array
     */
    public function events()
    {
        return [
            Controller::EVENT_AFTER_ACTION => 'log'
        ];
    }

    /**
     * @param ActionEvent $event
     * @return bool
     */
    public function log($event)
    {
        if ((is_array($this->loggingEvent)
                && !empty($this->loggingEvent)
                && !in_array($event->action->id, $this->loggingEvent))
            || (is_string($this->loggingEvent)
                && $this->loggingEvent !== $event->action->id)) {
            return true;
        }

        if ((is_array($this->exceptEvent)
                && in_array($event->action->id, $this->exceptEvent))
            || (is_string($this->exceptEvent) && $this->exceptEvent == $event->action->id)) {
            return true;
        }

        return Logger::log($event->action->controller->id, $event->action->id, $event->result);
    }
}