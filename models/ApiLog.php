<?php

namespace jekhe\logger\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "api_log".
 *
 * @property int $id
 * @property string $controller
 * @property string $action
 * @property int $statusCode
 * @property string $method
 * @property string $queryParams
 * @property array $requestHeaders
 * @property array $requestBody
 * @property array $responseBody
 * @property integer $created_at
 * @property integer $updated_at
 */
class ApiLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'api_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['statusCode', 'created_at', 'updated_at'], 'integer'],
            [['requestHeaders', 'requestBody', 'responseBody'], 'safe'],
            [['controller', 'action'], 'string', 'max' => 32],
            [['method'], 'string', 'max' => 10],
            [['queryParams'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'controller' => 'Controller Name',
            'action' => 'Action Name',
            'method' => 'Метод',
            'statusCode' => 'Код ответа',
            'queryParams' => 'Параметры запроса',
            'requestHeaders' => 'Заголовки запроса',
            'requestBody' => 'Тело запроса',
            'responseBody' => 'Ответ сервера',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }
}
