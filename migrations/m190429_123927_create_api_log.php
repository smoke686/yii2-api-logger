<?php

use yii\db\Migration;

/**
 * Class m190429_123927_create_api_log
 */
class m190429_123927_create_api_log extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->getTableSchema('{{%api_log}}', true)) {
            $this->dropTable('{{%api_log}}');
        }

        $this->createTable('{{%api_log}}', [
            'id' => $this->primaryKey(),
            'controller' => $this->string(64)->null()->comment('Controller Name'),
            'action' => $this->string(64)->null()->comment('Action Name'),
            'method' => $this->string(10)->null()->comment('Метод'),
            'statusCode' => $this->integer(4)->null()->comment('Код ответа'),
            'queryParams' => $this->string(255)->null()->comment('Параметры запроса'),
            'requestHeaders' => $this->json()->null()->comment('Заголовки запроса'),
            'requestBody' => $this->json()->null()->comment('Тело запроса'),
            'responseBody' => $this->json()->null()->comment('Ответ сервера'),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%api_log}}');
    }
}
